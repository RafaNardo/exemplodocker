FROM mcr.microsoft.com/dotnet/runtime:5.0

WORKDIR /app

COPY ./publish /app

ENV TESTE="VALOR TESTE"

ENTRYPOINT ["dotnet", "TesteDocker.dll"]