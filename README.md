# Exemplo de Docker para ajudar o Alex

  

## Como rodar


 - **Publicar a api**
	 - dotnet publish -o publish --self-contained -r  linux-x64  
 - **Buildar a imagem**
	 - docker image build -t api-linda-maravilhosa .
 - **Rodar a imagem**
	 - docker container run -p 8080:80 api-linda-maravilhosa
 - **Executar um service do SWARM**
	 - docker stack deploy -c .\docker-compose.yml [NOME DO SERVICE]

  
Feito